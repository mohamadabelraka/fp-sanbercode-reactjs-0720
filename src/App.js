import React, { useEffect } from 'react';
import Main from './layouts/Main';
import './App.css';
import {UserProvider} from "./context/UserContext"

function App() {
  useEffect(() => {
    document.title= "Review Movies & Games"

  })
  return (
    <>
      <UserProvider>
        <Main />
      </UserProvider>
    </>
  );
}

export default App;
