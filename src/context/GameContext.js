import React, { useState, useEffect, createContext } from "react";
import axios from 'axios';

export const GameContext = createContext();
export const GameProvider = props => {
    const [gameList, setGameList] = useState(null)

    useEffect(() => {
        if (gameList === null) {
            axios.get(`https://backendexample.sanbersy.com/api/games`)
                .then(response => {
                    setGameList(response.data.map(item => {
                        return {
                            id: item.id,
                            created_at: item.created_at,
                            updated_at: item.updated_at,
                            name: item.name,
                            genre: item.genre,
                            singlePlayer: item.singlePlayer,
                            multiplayer: item.multiplayer,
                            platform: item.platform,
                            release: item.release,
                            image_url: item.image_url
                        }
                    }))

                })
        }

    }, [gameList])

    return (
        <GameContext.Provider value={[gameList, setGameList]}>
            {props.children}
        </GameContext.Provider>
    );
};