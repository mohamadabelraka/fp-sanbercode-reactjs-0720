import React, { useState, useEffect, createContext } from "react";
import axios from 'axios';

export const MovieContext = createContext();
export const MovieProvider = props => {
    const [movieList, setMovieList] = useState(null)

    useEffect(() => {
        if (movieList === null) {
            axios.get(`https://backendexample.sanbersy.com/api/movies`)
                .then(response => {
                    setMovieList(response.data.map(item => {
                        return {
                            id: item.id,
                            created_at: item.created_at,
                            updated_at: item.updated_at,
                            title: item.title,
                            genre: item.genre,
                            description: item.description,
                            year: item.year,
                            duration: item.duration,
                            rating: item.rating,
                            review: item.review,
                            image_url: item.image_url
                        }
                    }))

                })
        }

    }, [movieList])

    return (
        <MovieContext.Provider value={[movieList, setMovieList]}>
            {props.children}
        </MovieContext.Provider>
    );
};