import React, { useState, createContext, useEffect } from "react";

export const UserContext = createContext();
export const UserProvider = props => {
  const [user, setUser] = useState(null)

  useEffect(() => {
    if (localStorage.getItem('user')) {
      setUser({ id: JSON.parse(localStorage.getItem('user')).id, username: JSON.parse(localStorage.getItem('user')).username})
    }
  }, [])


  return (
    <UserContext.Provider value={[user, setUser]}>
      {props.children}
    </UserContext.Provider>
  );
}