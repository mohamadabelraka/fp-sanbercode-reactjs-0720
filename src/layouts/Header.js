import React, { useContext } from "react"
import { Link, useHistory } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import {Avatar} from '@material-ui/core/';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';



const Header =() =>{
  const [user, setUser] = useContext(UserContext)
  const handleLogout = () =>{
    setUser(null)
    localStorage.removeItem("user")
  }
  const history = useHistory()
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const multipleOnClick = () =>{
    handleClose()
    handleLogout()
  }
  const changePassword = () =>{
    handleClose()
    history.push('/change_password')
  }

  return(    
    <header>
      <div>
        <Link to="/"><img id="logo" alt="Movge" src="/img/kisspng-roll-film-logo-cinema-roll-5abe468859c0a0.4946769715224193363676.png" width="70px" /></Link>
      </div>
      <nav>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/about">About </Link> </li>
          { user && <li><Link to="/movies/list">Movie List Editor </Link></li> }
          {user && <li><Link to="/games/list">Game List Editor </Link></li>}
          { user === null && <li><Link to="/login">Login </Link></li> }
          { user && <li>
              <Avatar src="/broken-image.jpg" onClick={handleClick} style={{width:25, height:25}}/>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
              
            >
              <MenuItem style={{ fontFamily: 'cursive' }} onClick={changePassword}>Change Password</MenuItem>
              <MenuItem style={{ fontFamily: 'cursive' }} onClick={multipleOnClick}>Logout</MenuItem>
            </Menu>
          </li> }
        </ul>
      </nav>
    </header>
  )
}

export default Header