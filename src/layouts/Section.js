import React, { useContext} from "react"
import {
  Switch,
  Route,
  Redirect,
  useHistory
} from "react-router-dom";
import About from "../pages/About"
import ChangePassword from "../pages/User/UserChangePass"
import Movies from "../pages/Movie/Movies"
import Login from "../pages/User/Login"
import UserRegister from "../pages/User/UserRegister"
import {UserContext} from "../context/UserContext"
import DetailMovie from "../pages/Movie/DetailMovies"
import MovieHome from "../pages/Movie/MovieHome"
import Home from "../pages/Home/Home"
import Game from "../pages/Game/Game"
import { GameProvider } from "../context/GameContext";
import DetailGame from "../pages/Game/DetailGame"
import GameCreate from "../pages/Game/CreateGame";
import { MovieProvider } from "../context/MovieContext";
import MovieForm from "../pages/Movie/MovieForm";
import GameHome from "../pages/Game/GameHome";

const Section = () =>{

  const [user, setUser] = useContext(UserContext);
  const history = useHistory();
  const PrivateRoute = ({ component: Component, users, ...rest }) => {
    return (
      <Route
        {...rest}
        render={(props) => localStorage.getItem('user') ?
          <Component {...props} />
          : <Redirect to={{ pathname: "/login", state: { from: props.location.pathname } }} />
        }
      />
    )
  };

  const LoginRoute = ({user, ...props }) =>
  user ? <Redirect to="/" /> : <Route {...props} />;
  const handleLogout = () => {
    setUser({ isLogin: false, id: "" })
    localStorage.removeItem('user')
    history.push("/login");
  }


  return(    
    <section >
      <Switch>
        <Route exact path="/" user={user} component={Home}/>
        <Route exact path="/about" user={user} component={About}/>
        <LoginRoute exact path="/login" user={user} component={Login}/>
        <Route exact path="/register" component={UserRegister} />
        <PrivateRoute exact path="/change_password" user={user} component={ChangePassword} />
        <Route exact path="/logout" user={user}>
          {handleLogout}
        </Route>

        <MovieProvider>
          <GameProvider>
            <Route exact path="/games" user={user} component={GameHome} />
            <PrivateRoute exact path="/games/list" user={user} component={Game} />
            <Route exact path="/games/:id" user={user} component={DetailGame} />
            <PrivateRoute exact path="/games/edit/:id" user={user} component={GameCreate} />
            <PrivateRoute exact path="/games/create" user={user} component={GameCreate} />
          </GameProvider>        
          <Route exact path="/movies" user={user} component={MovieHome} />
          <PrivateRoute exact path="/movies/list" user={user} component={Movies} />
          <PrivateRoute exact path="/movies/create" user={user} component={MovieForm} />
          <Route exact path="/movies/:id" user={user} component={DetailMovie} />
          <PrivateRoute exact path="/movies/edit/:id" user={user} component={MovieForm} />
        </MovieProvider>
        
        
      </Switch>
      
    </section>
  )
}

export default Section