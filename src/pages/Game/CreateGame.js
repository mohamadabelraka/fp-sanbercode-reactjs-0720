import React, { useState, useEffect } from "react"
import axios from 'axios';
import { useHistory, useParams, Link } from "react-router-dom";
import { Grid, Button, TextField, makeStyles } from '@material-ui/core';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            marginBottom: theme.spacing(2),

        },
    },
}));

const GameCreate = () => {
    let { id } = useParams()
    const classes = useStyles();
    const history = useHistory();

    const [inputGame, setInputGame] = useState({})
    useEffect(() => {
        if (inputGame.name === undefined && id !== undefined) {
            axios.get(`https://backendexample.sanbersy.com/api/games/${id}`)
                .then(res => {
                    let el = res.data
                    setInputGame({
                        created_at: el.created_at,
                        updated_at: el.updated_at,
                        name: el.name,
                        genre: el.genre,
                        singlePlayer: el.singlePlayer,
                        multiplayer: el.multiplayer,
                        platform: el.platform,
                        release: el.release,
                        image_url: el.image_url
                    })

                })
        }

    }, [inputGame])

    const handleChange = (event) => {
        let typeOfInput = event.target.name

        switch (typeOfInput) {
            case "name":
                {
                    setInputGame({ ...inputGame, name: event.target.value });
                    break
                }
            case "genre":
                {
                    setInputGame({ ...inputGame, genre: event.target.value });
                    break
                }
            case "singlePlayer":
                {
                    setInputGame({ ...inputGame, singlePlayer: event.target.value });
                    break
                }
            case "multiplayer":
                {
                    setInputGame({ ...inputGame, multiplayer: event.target.value });
                    break
                }
            case "platform":
                {
                    setInputGame({ ...inputGame, platform: event.target.value });
                    break
                }
            case "release":
                {
                    setInputGame({ ...inputGame, release: event.target.value });
                    break
                }
            case "image_url":
                {
                    setInputGame({ ...inputGame, image_url: event.target.value });
                    break
                }
            default:
                { break; }
        }
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        console.log("tes handlesubmit")
        let name = event.target.name.value
        let genre = event.target.genre.value
        let singlePlayer = event.target.singlePlayer.value
        let multiplayer = event.target.multiplayer.value
        let platform = event.target.platform.value
        let release = event.target.release.value
        let image_url = event.target.image_url.value

        var today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();

        let updated_at = date

        if (name.replace(/\s/g, '') !== "" && genre.replace(/\s/g, '') !== "" && singlePlayer.replace(/\s/g, '') !== "" && multiplayer.replace(/\s/g, '') !== "" && platform.toString().replace(/\s/g, '') !== "" && release.toString().replace(/\s/g, '') !== "") {
            if (id === undefined) {
                axios.post(`https://backendexample.sanbersy.com/api/games`, { name, genre, singlePlayer, multiplayer, platform, release, image_url })
                    .then(res => {
                        console.log("input sukses")
                    })
            } else if (id !== undefined) {
                axios.put(`https://backendexample.sanbersy.com/api/games/${id}`, { name, genre, singlePlayer, multiplayer, platform, release, updated_at })
                    .then(() => {
                        alert("Data berhasil di edit")
                    })
            }

            history.push("/games/list");
        }

    }

    return (
        <>
            <h1>Form Review Games</h1>
            {inputGame !== null &&
                <>
                    <form className={classes.root} autoComplete="off" onSubmit={handleSubmit}>
                    <Link to={'/games/list'}><KeyboardBackspaceIcon /></Link>
                        <Grid container spacing={3}>
                            <Grid item sm={6}>
                                <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Judul Game" variant="outlined" name="name" value={inputGame.name} onChange={handleChange} />
                            <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Genre" variant="outlined" name="genre" value={inputGame.genre} onChange={handleChange}/>

                                <FormControl component="fieldset">
                                    <FormLabel component="legend">Single Player?</FormLabel>
                                    <RadioGroup aria-label="singlePlayer" name="singlePlayer" defaultValue={inputGame.singlePlayer === "1" ? "1" : "0"} onChange={handleChange}>
                                        <FormControlLabel value="1" control={<Radio />} label="Ya" />
                                        <FormControlLabel value="0" control={<Radio />} label="Tidak" />
                                    </RadioGroup>
                                </FormControl>

                            </Grid>
                            <Grid item sm={6}>
                            <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Platform" variant="outlined" name="platform" value={inputGame.platform} onChange={handleChange}/>
                            <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" type="number" label="Tahun Rilis" variant="outlined" name="release" value={inputGame.release} onChange={handleChange}/>
                                <FormControl component="fieldset">
                                    <FormLabel component="legend">Multi Player?</FormLabel>
                                    <RadioGroup aria-label="multiplayer" name="multiplayer" defaultValue={inputGame.multiplayer === "1" ? "1" : "0"} onChange={handleChange}>
                                        <FormControlLabel value="1" control={<Radio />} label="Ya" />
                                        <FormControlLabel value="0" control={<Radio />} label="Tidak" />
                                    </RadioGroup>
                                </FormControl>

                            </Grid>
                            <Grid item sm={12}>
                            <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Url Gambar" variant="outlined" name="image_url" value={inputGame.image_url} onChange={handleChange}/>

                            </Grid>
                        </Grid>

                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Submit
        </Button>
                    </form>
                </>
            }
        </>
    )

}

export default GameCreate