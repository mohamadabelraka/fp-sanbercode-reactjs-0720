import React, { Component } from "react";
import axios from 'axios';
import { Grid } from "@material-ui/core";

class DetailGame extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            game: null
        }
    }

    componentDidMount() {
        axios.get(`https://backendexample.sanbersy.com/api/games/${this.state.id}`)
            .then(res => {
                let game = {
                    image_url: res.data.image_url,
                    created_at: res.data.created_at,
                    updated_at: res.data.updated_at,
                    name: res.data.name,
                    genre: res.data.genre,
                    singlePlayer: res.data.singlePlayer,
                    multiplayer: res.data.multiplayer,
                    platform: res.data.platform,
                    release: res.data.release
                }
                this.setState({ game })
            })
    }

    render() {
        return (
            <>
                {
                    this.state.game !== null &&
                    <div>
                        <h2>{this.state.game.name}</h2>
                        <Grid container spacing={2}>
                            <Grid item sm={3} xs={12}>

                                <img src={this.state.game.image_url} alt="" style={{ width: "180px", height: "240px", border: "5px solid #555" }} onError={(e) => { e.target.onerror = null; e.target.src = "../public/img/no_image.jpg" }} />

                            </Grid>
                            <Grid item sm={9} xs={12}>
                                <p><strong>Genre :</strong> {this.state.game.genre} </p>

                                <p><strong>Single Player :</strong> {this.state.game.singlePlayer === 1 ? `Ya` : `Tidak`}</p>
                                <p><strong>Multi Player :</strong> {this.state.game.multiplayer === 1 ? `Ya` : `Tidak`}</p>
                                <p><strong>Platform : </strong> {this.state.game.platform}</p>
                                <p><strong>Tahun Rilis : </strong> {this.state.game.release}</p>

                            </Grid>
                        </Grid>
                    </div>
                }
            </>
        )
    }
}

export default DetailGame
