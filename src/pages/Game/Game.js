import React from "react"
import GameList from "./GameList"
import GameFilter from "./FilterGame"

const Game = () => {

    return (
        <>
            <GameFilter/>
            <GameList/>
        </>
    )
}

export default Game