import React from "react"
import HomeMovie from './HomeMovie'
import HomeGame from './HomeGame'

const Home = () => {
    return (
        <>
            <HomeMovie/>
            <HomeGame/>
        </>
    )
}

export default Home