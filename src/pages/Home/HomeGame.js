import React, { Component } from "react"
import axios from "axios"

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import { CardMedia } from "@material-ui/core";
import { Link } from "react-router-dom";


class HomeGame extends Component {
    constructor(props) {
        super(props)
        this.state = {
            games: []
        }
    }

    componentDidMount() {
        axios.get(`https://www.backendexample.sanbersy.com/api/games`)
            .then(res => {
                let games = res.data.map(item => {
                    return {
                        id: item.id,
                        created_at: item.created_at,
                        updated_at: item.updated_at,
                        name: item.name,
                        genre: item.genre,
                        singlePlayer: item.singlePlayer,
                        multiplayer: item.multiplayer,
                        platform: item.platform,
                        release: item.release,
                        image_url: item.image_url
                    }
                })
                this.setState({ games })
            })
    }

    render() {
        return (

            <>                
                <h1>Game List</h1>
                {this.state.games
                    .reverse()
                    .slice(this.state.games.length - 5, this.state.games.length)
                    .map((el, index) => {
                        return (

                            <CardActionArea component="a" href={`/games/${el.id}`}>
                                <Card>
                                    <Grid container spacing={2}>
                                        <Grid item sm={3} xs={12}>
                                            <CardMedia>
                                                <img src={el.image_url} alt="" style={{ width: "180px", height: "240px", border: "5px solid #555" }} />
                                            </CardMedia>
                                        </Grid>
                                        <Grid item sm={9} xs={12}>
                                            <CardContent>
                                                <Typography component="h2" variant="h5" color='primary'>
                                                    {el.name}
                                                </Typography>
                                                <Typography variant="subtitle1">
                                                    Release : {el.release}
                                                </Typography>
                                                <Typography variant="subtitle1">
                                                    Platform : {el.platform}
                                                </Typography>
                                                <Typography variant="subtitle1">
                                                    Genre : {el.genre}
                                                </Typography>

                                            </CardContent>
                                        </Grid>
                                    </Grid>
                                </Card>
                            </CardActionArea>

                        )

                    })}
                <h3 style={{ textAlign: 'end' }}><Link to='/games'>Show More</Link></h3>
            </>
        )
    }
}

export default HomeGame