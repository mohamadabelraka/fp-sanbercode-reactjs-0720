import React, { Component } from "react";
import axios from 'axios';
import { Grid } from "@material-ui/core";
import {Rating} from '@material-ui/lab'
class DetailMovie extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            movie: null
        }

        
    }
    componentDidMount() {
        axios.get(`https://backendexample.sanbersy.com/api/movies/${this.state.id}`)
            .then(res => {
                let movie = {

                    title: res.data.title,
                    description: res.data.description,
                    year: res.data.year,
                    rating: res.data.rating,
                    duration: res.data.duration,
                    genre: res.data.genre,
                    review: res.data.review,
                    created_at: res.data.created_at,
                    updated_at: res.data.updated_at,
                    image_url: res.data.image_url
                }
                this.setState({ movie })
            })
    }

    render() {
        return (
            <>
                {
                    this.state.movie !== null &&
                    <div>
                        <h1>{this.state.movie.title} ( {this.state.movie.year} )</h1>
                        <Grid container spacing={2}>
                            <Grid item sm={3} xs={12}>
                                <img src={this.state.movie.image_url} alt="" style={{ width: "180px", height: "240px", border: "5px solid #555" }} />
                            </Grid>
                            <Grid item sm={9} xs={12}>
                                <h3>Rating : </h3>
                                <Rating name="customized-10" defaultValue={0} max={10} value={this.state.movie.rating} readOnly />
                                <p><strong>Durasi :</strong> {this.state.movie.duration} menit</p>
                                <p><strong>Genre :</strong> {this.state.movie.genre} </p>
                                <p><strong>Deskripsi : </strong> <br/> {this.state.movie.description}</p>
                                <p><strong>Review : </strong> <br />{this.state.movie.review}</p>
                            </Grid>
                        </Grid>
                    </div>
                }
            </>
        )
    }
}

export default DetailMovie
