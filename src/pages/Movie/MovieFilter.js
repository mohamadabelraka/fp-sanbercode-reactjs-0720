import React, { useContext, useState } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";
import { MovieContext } from '../../context/MovieContext';

const useStyles = makeStyles((theme) => ({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
    root: {
        '& .MuiTextField-root': {
            marginBottom: theme.spacing(2),
            marginRight: theme.spacing(2),
        },
        width: 300
    },

}));

export default function MovieFilter() {
    const classes = useStyles();
    const theme = useTheme();
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });
    const [open, setOpen] = React.useState(false);
    const handleDrawerClose = () => {
        setOpen(false);
    };

    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const [movieList, setMovieList] = useContext(MovieContext)
    const [inputFilter, setInputFilter] = useState({})
    const [tempMovie, setTempMovie] = useState(null)

    if (movieList !== null && tempMovie === null) {
        setTempMovie(movieList)
    }

    const handleChange = (event) => {
        let typeOfInput = event.target.name

        switch (typeOfInput) {
            case "titleMovie":
                {
                    setInputFilter({ ...inputFilter, name: event.target.value });
                    break
                }
            case "genre":
                {
                    setInputFilter({ ...inputFilter, genre: event.target.value });
                    break
                }
            case "yearMin":
                {
                    setInputFilter({ ...inputFilter, yearMin: event.target.value });
                    break
                }
            case "yearMax":
                {
                    setInputFilter({ ...inputFilter, yearMax: event.target.value });
                    break
                }
            case "singlePlayer":
                {
                    setInputFilter({ ...inputFilter, singlePlayer: event.target.value });
                    break
                }
            case "multiplayer":
                {
                    setInputFilter({ ...inputFilter, multiplayer: event.target.value });
                    break
                }
            default:
                { break; }
        }
    }

    const handleFilter = (event) => {
        // menahan submit
        event.preventDefault()

        let title = event.target.title.value
        let genre = event.target.genre.value
        let yearMin = event.target.yearMin.value
        let yearMax = event.target.yearMax.value
        let rating = event.target.rating.value

        let filteredMovie = null
        if (title.replace(/\s/g, '') !== "") {
            filteredMovie = movieList.filter(el =>
                el.title.toLowerCase().includes(title.toLowerCase()))
        }

        if (genre.replace(/\s/g, '') !== "") {
            filteredMovie = movieList.filter(el =>
                el.genre.toLowerCase().includes(genre.toLowerCase()))
        }

        if (yearMin.replace(/\s/g, '') !== "") {
            filteredMovie = movieList.filter(el =>
                el.year >= yearMin)
        }

        if (yearMax.replace(/\s/g, '') !== "") {
            filteredMovie = movieList.filter(el =>
                el.year <= yearMax)
        }

        if (yearMax.replace(/\s/g, '') !== "" && yearMin.replace(/\s/g, '') !== "") {
            filteredMovie = movieList.filter(el =>
                el.year >= yearMin && el.year <= yearMax)
        }

        if (rating.replace(/\s/g, '') !== "") {
            filteredMovie = movieList.filter(el =>
                el.rating >= rating)
        }
        if (filteredMovie !== null) {
            setMovieList([...filteredMovie])

        }
    }

    const resetFilter = () => {
        console.log("temp")
        console.log(tempMovie)
        setMovieList(tempMovie)
        setInputFilter({})
    }

    return (
        <div>
            {['left'].map((anchor) => (
                <React.Fragment key={anchor}>
                    <center>

                        <Typography className={classes.title} variant="h4" id="tableTitle" component="div">
                            Movie List
                        </Typography>
                    </center>
                    <Button onClick={toggleDrawer(anchor, true)} color="primary" variant="contained">Filter Movies</Button>
                    <Button color="primary" variant="contained" style={{ marginBottom: "16px", float: "right" }}><Link to="/movies/create" style={{ color: "white" }}>Create Movie Review</Link></Button>

                    <Drawer anchor="left" open={state["left"]} onClose={toggleDrawer("left", false)}>
                        {/* {list(anchor)} */}
                        <div className={classes.drawerHeader}>
                            <IconButton onClick={handleDrawerClose}>
                                {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                            </IconButton>
                        </div>
                        <Divider />
                        {inputFilter !== null &&
                            <div style={{ padding: "16px" }}>
                                <h2>Filter</h2>
                                <form className={classes.root} autoComplete="off" onSubmit={handleFilter}>
                                    <TextField fullWidth size="small" InputLabelProps={{ shrink: true }} id="outlined-basic" label="Title" variant="outlined" name="title" onChange={handleChange} value={inputFilter.title} />
                                    <TextField fullWidth size="small" InputLabelProps={{ shrink: true }} id="outlined-basic" label="Genre" variant="outlined" name="genre" onChange={handleChange} value={inputFilter.genre} />
                                    <Divider />

                                    <h3>Tahun</h3>
                                    <TextField InputLabelProps={{ shrink: true }} id="outlined-basic" label="Awal" variant="outlined" name="yearMin" style={{ width: 100 }} type="number" onChange={handleChange} value={inputFilter.yearMin} />
                                    <h4 style={{ display: "inline" }}><strong>s/d</strong></h4>
                                    <TextField InputLabelProps={{ shrink: true }} id="outlined-basic" label="Akhir" variant="outlined" name="yearMax" style={{ width: 100, marginLeft: theme.spacing(2) }} type="number" onChange={handleChange} value={inputFilter.yearMax} />
                                    <Divider />
                                <TextField fullWidth size="small" InputLabelProps={{ shrink: true }} id="outlined-basic" label="Rating" variant="outlined" name="rating" onChange={handleChange} value={inputFilter.rating} />
                                    <Divider />

                                    <Button
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        fullWidth
                                        style={{ marginTop: theme.spacing(2) }}
                                    >
                                        Filter
                                    </Button>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        fullWidth
                                        style={{ marginTop: theme.spacing(1) }}
                                        onClick={resetFilter}
                                    >
                                        Reset
                                    </Button>
                                </form>
                            </div>
                        }
                    </Drawer>
                </React.Fragment>
            ))}
        </div>
    );
}