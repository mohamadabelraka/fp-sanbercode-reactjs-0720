import React, { useState, useEffect } from "react"
import axios from 'axios';
import { useHistory, useParams, Link } from "react-router-dom";
import { Grid, Button, TextField, makeStyles } from '@material-ui/core';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            marginBottom: theme.spacing(2),

        },
    },
}));

const MovieForm = () => {
    let {id} = useParams()
    const classes = useStyles();
    const history = useHistory();
    const [inputMovie, setInputMovie] = useState({})
    useEffect(() => {
        if (inputMovie.title === undefined && id !== undefined) {
            axios.get(`https://backendexample.sanbersy.com/api/movies/${id}`)
                .then(res => {
                    let item = res.data
                    setInputMovie({
                        created_at: item.created_at,
                        updated_at: item.updated_at,
                        title: item.title,
                        genre: item.genre,
                        description: item.description,
                        year: item.year,
                        duration: item.duration,
                        rating: item.rating,
                        review: item.review,
                        image_url: item.image_url
                    })

                })
        }

    }, [inputMovie])

    const handleChange = (event) => {
        let typeOfInput = event.target.name

        switch (typeOfInput) {
            case "title":
                {
                    setInputMovie({ ...inputMovie, title: event.target.value });
                    break
                } case "genre":
                {
                    setInputMovie({ ...inputMovie, genre: event.target.value });
                    break
                }
            case "description":
                {
                    setInputMovie({ ...inputMovie, description: event.target.value });
                    break
                }
            case "year":
                {
                    setInputMovie({ ...inputMovie, year: event.target.value });
                    break
                }
            case "duration":
                {
                    setInputMovie({ ...inputMovie, duration: event.target.value });
                    break
                }
            case "rating":
                {
                    setInputMovie({ ...inputMovie, rating: event.target.value });
                    break
                }
            case "review":
                {
                    setInputMovie({ ...inputMovie, review: event.target.value });
                    break
                }
            case "image_url":
                {
                    setInputMovie({ ...inputMovie, image_url: event.target.value });
                    break
                }
            default:
                { break; }
        }
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        let title = event.target.title.value
        let genre = event.target.genre.value
        let description = event.target.description.value
        let year = event.target.year.value
        let duration = event.target.duration.value
        let rating = event.target.rating.value
        let review = event.target.review.value
        let image_url = event.target.image_url.value

        var today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();

        let updated_at = date

        if (title.replace(/\s/g, '') !== "" && genre.replace(/\s/g, '') !== "" && year.replace(/\s/g, '') !== "" && description.replace(/\s/g, '') !== "" && duration.toString().replace(/\s/g, '') !== "" && review.toString().replace(/\s/g, '') !== "" && rating.toString().replace(/\s/g, '') !== "" && image_url.toString().replace(/\s/g, '') !== "") {
            if (id === undefined) {
                axios.post(`https://backendexample.sanbersy.com/api/movies`, { title, genre, description, year, duration, rating, review, image_url })
                    .then(res => {
                        alert("Data berhasil di input")
                    })
            } else if (id !== undefined) {
                axios.put(`https://backendexample.sanbersy.com/api/movies/${id}`, { title, genre, description, year, duration, rating, review, image_url, updated_at })
                    .then(() => {
                        alert("Data berhasil di edit")
                    })
            }

            history.push("/movies/list");
        }

    }

    return (
        <>
            <h1>Form Review Movies</h1>
            {inputMovie !== null &&
                <>
                    <form className={classes.root} autoComplete="off" onSubmit={handleSubmit}>
                    <Link to={'/movies/list'}><KeyboardBackspaceIcon/></Link>
                        <Grid container spacing={3}>
                            <Grid item sm={6}>
                                <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Title" variant="outlined" name="title" value={inputMovie.title} onChange={handleChange} />
                                <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Genre" variant="outlined" name="genre" value={inputMovie.genre} onChange={handleChange} />
                                <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Description" variant="outlined" name="description" value={inputMovie.description} onChange={handleChange} />
                                <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Year" variant="outlined" name="year" value={inputMovie.year} onChange={handleChange} />
                                <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Duration (Minutes)" variant="outlined" name="duration" value={inputMovie.duration} onChange={handleChange} />
                                <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Rating" variant="outlined" name="rating" value={inputMovie.rating} onChange={handleChange} />
                                <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Review" variant="outlined" name="review" value={inputMovie.review} onChange={handleChange} />
                            </Grid>
                            <Grid item sm={12}>
                                <TextField fullWidth InputLabelProps={{ shrink: true }} id="outlined-basic" label="Image URL" variant="outlined" name="image_url" value={inputMovie.image_url} onChange={handleChange} />

                            </Grid>
                        </Grid>

                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Submit
        </Button>
                    </form>
                </>
            }
        </>
    )

}

export default MovieForm