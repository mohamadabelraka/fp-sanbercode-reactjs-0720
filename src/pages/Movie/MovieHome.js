import React, { Component } from "react"
import axios from "axios"

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import { CardMedia } from "@material-ui/core";
import Rating from '@material-ui/lab/Rating'


class MovieHome extends Component {
    constructor(props) {
        super(props)
        this.state = {
            movies: []
        }
    }

    componentDidMount() {
        axios.get(`https://www.backendexample.sanbersy.com/api/movies`)
            .then(res => {
                let movies = res.data.map(el => {
                    return {
                        id: el.id,
                        title: el.title,
                        rating: el.rating,
                        duration: el.duration,
                        genre: el.genre,
                        description: el.description,
                        review: el.review,
                        image_url: el.image_url,
                        year: el.year
                    }
                })
                this.setState({ movies })
            })
    }

    render() {
        return (
            <>
                {this.state.movies.map((el, index) => {
                    return (
                        <CardActionArea component="a" href={`/movies/${el.id}`}>
                            <Card>
                                <Grid container spacing={2}>
                                    <Grid item sm={3} xs={12}>
                                        <CardMedia>
                                            <img src={el.image_url} alt="" style={{ width: "180px", height: "240px", border: "5px solid #555" }} />
                                        </CardMedia>
                                    </Grid>
                                    <Grid item sm={9} xs={12}>
                                        <CardContent>
                                            <Typography component="h2" variant="h5" color='primary'>
                                                {el.title}
                                            </Typography>
                                            <Typography variant="subtitle1" paragraph>
                                                Durasi : {el.duration} Menit
                                                    </Typography>
                                            <Typography variant="subtitle1" paragraph>
                                                Tahun : {el.year}
                                            </Typography>
                                            <Typography variant="subtitle1" paragraph>
                                                Genre : {el.genre}
                                            </Typography>
                                            <Typography component="legend" variant="subtitle1" paragraph>Rating: </Typography>                                             
                                            <Rating name="customized-10" defaultValue={0} max={10} value={el.rating} readOnly />

                                        </CardContent>
                                    </Grid>
                                </Grid>
                            </Card>
                        </CardActionArea>

                    )

                })}
            </>
        )
    }
}

export default MovieHome