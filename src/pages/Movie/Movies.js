import React from "react"
import MoviesList from "./MoviesList"
import MovieFilter from "./MovieFilter"

const Movies = () => {

    return (
        <>
            <MovieFilter/>
            <MoviesList/>
        </>
    )
}

export default Movies