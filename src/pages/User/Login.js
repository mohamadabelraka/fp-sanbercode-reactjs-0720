import React, { useState, useContext } from 'react';
import { UserContext } from "../../context/UserContext";
import { Link, useHistory } from "react-router-dom";
import axios from 'axios';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Login() {
  const classes = useStyles();
  const [, setUser] = useContext(UserContext)
  const [msg, setmsg] = useState("")
  const history = useHistory();
  console.log(localStorage.getItem('user'))

  const handleLogin = (event) => {
    event.preventDefault();
    let username = event.target.username.value
    let password = event.target.password.value

    if(username !== "" && password!== ""){
      axios.post(`https://backendexample.sanbersy.com/api/login`, { username, password })
        .then(res => {
          console.log(res.data)

          if (res.data !== "invalid username or password") {
            setUser({ username: res.data.username, id: res.data.id })
            
            localStorage.setItem('user', res.data.id);

            history.push("/");
          } else {
            setmsg("username atau password salah")
          }
        })
        .catch(res => {
          console.log(res)
        })
    }
    else{
      setmsg("username dan password tidak boleh kosong")
    }

  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign in
                </Typography>

        <form className={classes.form} noValidate onSubmit={handleLogin}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"


          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"

          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            className={classes.submit}
          >
            Login
                    </Button>
          {msg !== null &&
            <h5 style={{ color: "red" }}>{msg} </h5>

          }
          <Grid container >

            <Grid item >
              <p>Register <Link to="/register">Here!</Link></p>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}