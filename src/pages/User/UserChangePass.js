import React, { useState, useEffect } from 'react';
import {useHistory} from 'react-router-dom';
import axios from 'axios';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', 
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function ChangePassword() {
    const classes = useStyles();
    const [userData, setUserData] = useState(null)

    const [msg, setmsg] = useState("")
    let id = localStorage.getItem("user")

    useEffect(() => {
        if (userData === null) {
            axios.get(`https://backendexample.sanbersy.com/api/users/${id}`)
                .then(res => {
                    setUserData({
                        id,
                        updated_at: res.data.updated_at,
                        username: res.data.username,
                        password: res.data.password
                    })

                })
        }

    }, [userData])
    const history = useHistory()
    const handleSubmit = (event) => {
        event.preventDefault();
        let passOld = event.target.passOld.value
        let passNew = event.target.passNew.value
        
        if(passNew !=="" && passOld !== ""){
            if (passOld === userData.password) {
                axios.put(`https://backendexample.sanbersy.com/api/users/${id}`, { password: passNew })
                    .then(res => {
                        alert("Password berhasil diganti.")
                        history.push("/")
                    })
                    .catch(res => {
                        console.log(res)
                    })
            } else {
                setmsg("Password yang dimasukkan salah")
            }
        }
        else{
            setmsg("Password Tidak Boleh Kosong")
        }



    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            {userData !== null &&
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Ganti Password
                </Typography>
                    <Typography component="h3" variant="h5">
                        Username : {userData.username}
                    </Typography>

                    <form className={classes.form} noValidate onSubmit={handleSubmit}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="passOld"
                            label="Password Lama"
                            type="password"
                            id="passOld"

                        />
                        {msg !== null &&
                            <h5 style={{ color: "red" }}>{msg} </h5>

                        }
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="passNew"
                            label="Password Baru"
                            type="password"
                            id="passNew"
                        

                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="secondary"
                            className={classes.submit}
                        >
                            Ganti Password
                    </Button>

                    </form>
                </div>
            }
        </Container>
    );
}