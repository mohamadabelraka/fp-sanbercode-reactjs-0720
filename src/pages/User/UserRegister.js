import React, { useState } from 'react';
import { Link, useHistory } from "react-router-dom";
import axios from 'axios';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.primary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Register() {
    const classes = useStyles();
    const [notif, setnotif] = useState("")
    const history = useHistory();

    const handleRegister = (event) => {
        event.preventDefault();
        let username = event.target.username.value
        let password = event.target.password.value

        if(username !== "" && password !== ""){
            axios.post(`https://backendexample.sanbersy.com/api/users`, { username, password })
                .then(res => {
                    console.log("sukses")
                    console.log(res)


                    if (res.data !== "username already exists") {
                        console.log("masuk")

                        history.push("/login");
                    } else {
                        setnotif("username sudah ada")
                    }
                })
                .catch(res => {
                    console.log("ada error")
                    console.log(res)
                })
        }
        else{
            setnotif("Username and Password can not be empty!")
        }
        


    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Register
                </Typography>

                <form className={classes.form} noValidate onSubmit={handleRegister} method="post">
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required={true}
                        fullWidth
                        id="username"
                        label="Username"
                        name="username"
                        autoComplete="username"
                        autoFocus
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required={true}
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="secondary"
                        className={classes.submit}
                    >
                        Register
                    </Button>
                    {notif !== null &&
                        <p style={{ color: "red" }}>{notif} </p>

                    }
                    <Grid container>

                        <Grid item>
                            <p>Sudah punya akun? Login di <Link to="/login">sini</Link></p>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}